package test;

import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.junit.Assert;
import org.junit.Test;

import game.engine.GameEngineImpl;
import game.graphics.SpriteSheet;
import game.objects.blocks.Block;
import game.objects.entities.Player;
import game.physics.Direction;
import game.theme.Theme;
import game.theme.ThemeImpl;
import game.utilities.Position;
import game.world.World;
import game.world.WorldImpl;

/**
 * This class test some player features like input, movement and key/door collision.
 */
public class TestPlayer {

    private static final long ELAPSED_TIME = 500;
    private static final int SPACE = SpriteSheet.SPRITE_SIZE_IN_GAME * GameEngineImpl.FPS;
    private static final Position INITIAL_POSITION = new Position(1 * SpriteSheet.SPRITE_SIZE_IN_GAME,
                                                                  1 * SpriteSheet.SPRITE_SIZE_IN_GAME);
    private static final String THEME = "/KingdomHeartsTheme";
    private static final double SAFE_BLOCK_X_COORDINATE = 2.00;
    private static final double SAFE_BLOCK_Y_COORDINATE = 2.00;
    private static final int PLAYER_SPRITE_MOVE_LEFT = 5;

    private World world;

    /**
     * Tests the player's position.
     *
     * @throws UnsupportedAudioFileException 
     * @throws IOException 
     * @throws LineUnavailableException 
     */
    @Test
    public void testPlayerPosition() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        //World's initialization
        final Theme theme = new ThemeImpl(THEME);
        this.world = new WorldImpl(theme, null);
        //World graphic component closed
        world.getGraphicComponent().dispose();
        final Player player = world.getPlayer();
        //Player is not in position (0, 0)
        Assert.assertFalse(player.getPosition().equals(new Position(0, 0)));
        //Player is in position (1, 1)
        Assert.assertTrue(player.getPosition().equals(INITIAL_POSITION));
        //Player is not moving because there's no input
        player.getPhysics().move(ELAPSED_TIME);
        Assert.assertTrue(player.getPosition().equals(new Position(INITIAL_POSITION)));
        //Player was moved in position (2, 1)
        player.getPosition().setX(2 * SpriteSheet.SPRITE_SIZE_IN_GAME);
        Assert.assertFalse(player.getPosition().equals(new Position(INITIAL_POSITION)));
        Assert.assertTrue(player.getPosition().equals(new Position(2 * SpriteSheet.SPRITE_SIZE_IN_GAME,
                                                                   1 * SpriteSheet.SPRITE_SIZE_IN_GAME)));
        //Player was moved in position (2, 2)
        player.getPosition().setY(2 * SpriteSheet.SPRITE_SIZE_IN_GAME);
        Assert.assertTrue(player.getPosition().equals(new Position(2 * SpriteSheet.SPRITE_SIZE_IN_GAME,
                                                                   2 * SpriteSheet.SPRITE_SIZE_IN_GAME)));
    }

    /**
     * Tests the key and door position.
     *
     * @throws UnsupportedAudioFileException 
     * @throws IOException 
     * @throws LineUnavailableException 
     */
    @Test
    public void testTakeKey() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        //World's initialization
        final Theme theme = new ThemeImpl(THEME);
        this.world = new WorldImpl(theme, null);
        //World graphic component closed
        world.getGraphicComponent().dispose();
        final Player player = world.getPlayer();
        //Initial player's position it must be different to the key one
        Assert.assertTrue(!world.getKey().get().getPosition().equals(world.getPlayer().getPosition()));
        //Player is moved on the key
        player.getPosition().setX(world.getKey().get().getPosition().getX());
        player.getPosition().setY(world.getKey().get().getPosition().getY());
        //Checks if the player's position is the same of the key one, if it is, the door is open
        if (player.getPosition().equals(world.getKey().get().getPosition())) {
            world.getDoor().open();
        }
        Assert.assertTrue(world.getDoor().isOpen());
    }

    /**
     * Tests the player input.
     *
     * @throws UnsupportedAudioFileException 
     * @throws IOException 
     * @throws LineUnavailableException 
     */
    @Test
    public void testPlayerInput() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
       //World's initialization
        final Theme theme = new ThemeImpl(THEME);
        this.world = new WorldImpl(theme, null);
        final Player player = world.getPlayer();
        //World graphic component closed
        world.getGraphicComponent().dispose();
        //Checks if player is not moving
        Assert.assertTrue(player.getPhysics().getVelocity().getDirection().equals(Direction.STOP));
        //Simulates an input command
        player.getInput().createDirectionCommand(Direction.RIGHT, SPACE);
        player.processInput();
        player.update(ELAPSED_TIME);
        //Checks if player is moved from his starting position
        Assert.assertFalse(player.getPosition().equals(INITIAL_POSITION));
        //Simulates another input command
        player.getInput().createDirectionCommand(Direction.LEFT, SPACE);
        player.processInput();
        player.update(ELAPSED_TIME);
        //Checks if player is back to the starting position
        Assert.assertTrue(player.getPosition().equals(INITIAL_POSITION));
    }

    /**
     * Tests a wrong index in player sprites list.
     *
     * @throws UnsupportedAudioFileException 
     * @throws IOException 
     * @throws LineUnavailableException 
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void testTheme() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        final Theme theme = new ThemeImpl(THEME);
        theme.getSprites().getEnemySprite().getLeftSprites().get(PLAYER_SPRITE_MOVE_LEFT);
    }

    /**
     * Tests a collision with a block.
     *
     * @throws UnsupportedAudioFileException 
     * @throws IOException 
     * @throws LineUnavailableException 
     */
    @Test
    public void testCollisions() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
       //World's initialization
        final Theme theme = new ThemeImpl(THEME);
        this.world = new WorldImpl(theme, null);
        //World graphic component closed
        world.getGraphicComponent().dispose();
        final Player player = world.getPlayer();
        //Moving the players in a new position
        player.getPosition().setX(2 * SpriteSheet.SPRITE_SIZE_IN_GAME);
        player.getPosition().setY(2 * SpriteSheet.SPRITE_SIZE_IN_GAME);
        final Block block = world.getBlocks().stream()
                                             .filter(b -> b.getPosition()
                                                           .equals(new Position(SAFE_BLOCK_X_COORDINATE
                                                                                * SpriteSheet.SPRITE_SIZE_IN_GAME,
                                                                                SAFE_BLOCK_Y_COORDINATE
                                                                                * SpriteSheet.SPRITE_SIZE_IN_GAME)))
                                                           .findFirst().get();
        Assert.assertTrue(player.getPhysics().checkGenericCollision(block));
        //Movintg the player in his starting position
        player.getPosition().setX(INITIAL_POSITION.getX());
        player.getPosition().setX(INITIAL_POSITION.getY());
        Assert.assertFalse(player.getPhysics().checkGenericCollision(block));
    }

}
