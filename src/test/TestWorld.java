package test;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.junit.Assert;
import org.junit.Test;

import game.graphics.SpriteSheet;
import game.maps.GameMap;
import game.maps.HorizontalMap;
import game.maps.MapGeneratorImpl;
import game.maps.NormalMap;
import game.maps.VerticalMap;
import game.object.Solidity;
import game.objects.blocks.Block;
import game.objects.blocks.Bomb;
import game.objects.blocks.Door;
import game.objects.blocks.PickableObject;
import game.objects.entities.DynamicObject;
import game.objects.entities.Enemy;
import game.theme.Theme;
import game.theme.ThemeImpl;
import game.utilities.GameTimer;
import game.utilities.Position;
import game.world.GameObjectFactoryImpl;
import game.world.World;
import game.world.WorldImpl;
import game.world.WorldInitializer;
import game.world.WorldInitializerImpl;

/**
 * Tests the correct implementation of the world.
 */
public class TestWorld {

    /**
     * Tests the correct implementation of tiles in the world.
     * 
     * @throws LineUnavailableException 
     * @throws IOException 
     * @throws UnsupportedAudioFileException 
     */
    @Test
    public void testTiles() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        //World's initialization
        final Theme theme = new ThemeImpl("/ZeldaTheme");
        final World world = new WorldImpl(theme, null);
        //World graphic component closed
        world.getGraphicComponent().dispose();
        //Blocks' positions test
        final Set<Block> blockSet = world.getBlocks();
        Assert.assertFalse(world.getFreePosition().stream()
                                                   .filter(p -> blockSet.stream()
                                                                        .filter(b -> b.getPosition().equals(p))
                                                                        .findAny()
                                                                        .isPresent())
                                                   .findAny()
                                                   .isPresent());
        final List<Block> blockList = new LinkedList<>(blockSet);
        final Position blockPosition = new Position(blockList.get(0).getPosition());
        //Unchanged block position test
        blockPosition.setX(blockPosition.getX() + 100);
        blockPosition.setY(blockPosition.getY() + 100);
        Assert.assertNotEquals(blockPosition.getX(), blockList.get(0).getPosition().getX(), 0);
        Assert.assertNotEquals(blockPosition.getY(), blockList.get(0).getPosition().getY(), 0);
        //Changed player position test
        final Position playerPosition = world.getPlayer().getPosition();
        Assert.assertTrue(playerPosition.equals(new Position(SpriteSheet.SPRITE_SIZE_IN_GAME, 
                                                             SpriteSheet.SPRITE_SIZE_IN_GAME)));
        playerPosition.setX(playerPosition.getX() + 100);
        playerPosition.setY(playerPosition.getY() + 100);
        Assert.assertEquals(world.getPlayer().getPosition().getX(), playerPosition.getX(), 0);
        Assert.assertEquals(world.getPlayer().getPosition().getY(), playerPosition.getY(), 0);
        //Key under breakable block test
        final Optional<PickableObject> key = world.getKey();
        Assert.assertTrue(blockSet.stream()
                                  .filter(b -> b.isBreakable() == Solidity.BREAKABLE)
                                  .filter(b -> b.getPosition().equals(key.get().getPosition()))
                                  .findAny().isPresent());
        Assert.assertFalse(blockSet.stream()
                                   .filter(b -> b.isBreakable() == Solidity.UNBREAKABLE)
                                   .filter(b -> b.getPosition().equals(key.get().getPosition()))
                                   .findAny().isPresent());
        //Door under breakable block test
        final Door door = world.getDoor();
        Assert.assertTrue(blockSet.stream()
                                  .filter(b -> b.isBreakable() == Solidity.BREAKABLE)
                                  .filter(b -> b.getPosition().equals(door.getPosition()))
                                  .findAny().isPresent());
        Assert.assertFalse(blockSet.stream()
                                   .filter(b -> b.isBreakable() == Solidity.UNBREAKABLE)
                                   .filter(b -> b.getPosition().equals(door.getPosition()))
                                   .findAny().isPresent());
        //Key position different from door position test
        Assert.assertFalse(key.get().getPosition().equals(door.getPosition()));
        //Free Position added test
        final Block block = blockList.remove(0);
        world.removeObject(block);
        Assert.assertTrue(world.getFreePosition().stream()
                                                 .filter(p -> p.equals(block.getPosition()))
                                                 .findAny()
                                                 .isPresent());
        //Enemies positions test
        final WorldInitializer initializer = new WorldInitializerImpl(theme);
        final Set<Position> freePositions = initializer.initializeFreeTiles();
        initializer.initializeUnbreakableBlocks();
        freePositions.removeAll(initializer.initializeBreakableBlocks(freePositions)
                                           .stream()
                                           .map(b -> b.getPosition())
                                           .collect(Collectors.toSet()));
        final Set<Enemy> enemies = initializer.initializeEnemies(freePositions, world);
        for (final DynamicObject enemy : enemies) {
            Assert.assertFalse(enemies.stream()
                                      .filter(e -> !e.equals(enemy))
                                      .filter(e -> e.getPosition().equals(enemy.getPosition()))
                                      .findAny()
                                      .isPresent());
            Assert.assertTrue(freePositions.stream()
                                           .filter(p -> p.equals(enemy.getPosition()))
                                           .findAny()
                                           .isPresent());
        }
    }

    /**
     * Tests the timer used in the world.
     * 
     * @throws InterruptedException 
     */
    @Test
    public void testTimer() throws InterruptedException {
        //Timer's initialization 
        final GameTimer timer = new GameTimer();
        //It has to be stopped at the beginning
        Assert.assertTrue(timer.isStopped());
        Assert.assertTrue(timer.getTimeMillis() == 0);
        //Timer gets started
        timer.start();
        long time = System.currentTimeMillis();
        Assert.assertFalse(timer.isStopped());
        timer.start();
        Assert.assertFalse(timer.isStopped());
        timer.stop();
        Thread.sleep(1000);
        timer.start();
        timer.stop();
        time = System.currentTimeMillis() - time;
        Assert.assertTrue(timer.isStopped());
        //Timer's stop test
        Assert.assertNotEquals(timer.getTimeMillis(), time);
        timer.stop();
        Assert.assertTrue(timer.isStopped());
    }

    /**
     * Tests to remove all kind of object in the world.
     * 
     * @throws LineUnavailableException 
     * @throws IOException 
     * @throws UnsupportedAudioFileException 
     * @throws InterruptedException 
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testRemovals() throws UnsupportedAudioFileException, IOException, LineUnavailableException, InterruptedException {
        final Theme theme = new ThemeImpl("/ZeldaTheme");
        final World world = new WorldImpl(theme, null);
        world.getGraphicComponent().dispose();
        final Block block = new LinkedList<Block>(world.getBlocks()).removeFirst();
        world.getBlocks().remove(block);
        world.removeObject(block);
        Assert.assertFalse(world.getBlocks().contains(block));
        for (final Block b : world.getBlocks()) {
            world.removeObject(b);
        }
        Assert.assertTrue(world.getBlocks().isEmpty());
        world.removeObject(world.getPlayer());
        Assert.assertFalse(world.getPlayer() == null);
        world.removeObject(world.getDoor());
        Assert.assertFalse(world.getDoor() == null);
        world.removeObject(world.getKey().get());
        Assert.assertFalse(world.getKey().isPresent());
        final Bomb bomb = new Bomb(new Position(0, 0), null, world);
        world.addBomb(new Bomb(new Position(0, 0), null, world));
        Assert.assertFalse(world.getBombs().isEmpty());
        world.removeObject(bomb);
        Assert.assertTrue(world.getBombs().isEmpty());
        world.addBomb(bomb);
    }

    /**
     * Tests the random map generator.
     * 
     * @throws UnsupportedAudioFileException 
     * @throws IOException 
     * @throws LineUnavailableException 
     */
    @Test
    public void testRandomMap() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        final Theme theme = new ThemeImpl("/SonicTheme");
        final GameMap map = new MapGeneratorImpl(new GameObjectFactoryImpl(theme)).getMapType();
        final Set<Position> positionsBlock = map.getMapLayout().stream().map(b -> b.getPosition())
                                                                        .collect(Collectors.toSet());
        if (positionsBlock.contains(new Position(3 * SpriteSheet.SPRITE_SIZE_IN_GAME,
                                                 2 * SpriteSheet.SPRITE_SIZE_IN_GAME))) {
            Assert.assertTrue(map.getClass().equals(VerticalMap.class));
        } else if (positionsBlock.contains(new Position(2 * SpriteSheet.SPRITE_SIZE_IN_GAME,
                                                        3 * SpriteSheet.SPRITE_SIZE_IN_GAME))) {
            Assert.assertTrue(map.getClass().equals(HorizontalMap.class));
        } else {
            Assert.assertTrue(map.getClass().equals(NormalMap.class));
        }
    }

}
