package test;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.junit.Assert;
import org.junit.Test;

import game.engine.GameEngineImpl;
import game.graphics.SpriteSheet;
import game.input.RandomInputComponent;
import game.input.graph.Graph;
import game.input.graph.GraphImpl;
import game.objects.blocks.Block;
import game.objects.blocks.Bomb;
import game.objects.entities.BalloomEnemy;
import game.objects.entities.GhostEnemy;
import game.objects.entities.SmartEnemy;
import game.physics.Direction;
import game.theme.Theme;
import game.theme.ThemeImpl;
import game.utilities.Position;
import game.world.World;
import game.world.WorldImpl;

/**
 * Tests principal features of enemies.
 */
public class TestEnemies {

    private static final int SECOND_TO_MILLISECOND = 1000;
    private static final int FPS = GameEngineImpl.FPS;
    private static final int GAME_SCALE = SpriteSheet.SPRITE_SIZE_IN_GAME;
    private static final double GHOST_ENEMY_START_X_COORDINATE = 1.40;
    private static final double GHOST_ENEMY_START_Y_COORDINATE = 1.00;
    private static final double SMART_ENEMY_START_X_COORDINATE = 5.00;
    private static final double SMART_ENEMY_START_Y_COORDINATE = 1.00;
    private static final double BALLOOM_ENEMY_START_X_COORDINATE = 2.60;
    private static final double BALLOOM_ENEMY_START_Y_COORDINATE = 1.80;
    private static final double GHOST_ENEMY_NEW_X_COORDINATE = 5.00;
    private static final double SMART_ENEMY_NEW_X_COORDINATE = 2.00;
    private static final double SMART_ENEMY_NEW_Y_COORDINATE = 1.60;
    private static final double BALLOOM_ENEMY_NEW_Y_COORDINATE = 1.00;
    private static final double BOMB_X_COORDINATE = 2.00;
    private static final double BOMB_Y_COORDINATE = 2.00;
    private static final double SAFE_BLOCK_X_COORDINATE = 3.00;
    private static final double SAFE_BLOCK_Y_COORDINATE = 1.00;

    /**
     * Test graph implementation.
     */
    @Test
    public void testGraph() {
        //Graph initialization
        final Graph graph = new GraphImpl();
        //Add node test
        Assert.assertFalse(graph.containsNode(new Position(0, 0)));
        graph.addNode(new Position(0, 0));
        graph.addNode(new Position(0, 1 * GAME_SCALE));
        graph.addNode(new Position(0, 2 * GAME_SCALE));
        graph.addNode(new Position(0, 3 * GAME_SCALE));
        graph.addNode(new Position(1 * GAME_SCALE, 1 * GAME_SCALE));
        //Contain node test
        Assert.assertTrue(graph.containsNode(new Position(0, 0)));
        Assert.assertTrue(graph.containsNode(new Position(0, 1 * GAME_SCALE)));
        Assert.assertTrue(graph.containsNode(new Position(0, 2 * GAME_SCALE)));
        Assert.assertTrue(graph.containsNode(new Position(1 * GAME_SCALE, 1 * GAME_SCALE)));
        //Path find test and add edge test
        graph.addEdge(new Position(0, 0), new Position(0, 1 * GAME_SCALE), Direction.UP);
        graph.addEdge(new Position(0, 1 * GAME_SCALE), new Position(0, 2 * GAME_SCALE), Direction.UP);
        Assert.assertFalse(graph.search(new Position(0, 0 * GAME_SCALE),
                                        new Position(0, 2 * GAME_SCALE)).isEmpty());
        final List<Direction> directions = new ArrayList<>();
        directions.addAll(Arrays.asList(Direction.UP, Direction.UP));
        Assert.assertEquals(directions, graph.search(new Position(0, 0 * GAME_SCALE),
                                                     new Position(0, 2 * GAME_SCALE)));
        directions.clear();
        graph.addEdge(new Position(0, 1 * GAME_SCALE),
                      new Position(1 * GAME_SCALE, 1 * GAME_SCALE), Direction.RIGHT);
        directions.addAll(Arrays.asList(Direction.UP, Direction.RIGHT));
        Assert.assertEquals(directions, graph.search(new Position(0, 0),
                                                     new Position(1 * GAME_SCALE, 1 * GAME_SCALE)));
    }

    /**
     * Test enemy movement.
     */
    @Test
    public void testEnemiesBehaviour() {
        //Enemy move test.
        final BalloomEnemy balloom = new BalloomEnemy(new Position(1 * GAME_SCALE, 1 * GAME_SCALE), null, null, null);
        final double distance = (balloom.getVelocity() * GAME_SCALE) * FPS;
        final RandomInputComponent randComponent = new RandomInputComponent(balloom, distance);
        final Position previousPos = new Position(balloom.getPosition());
        randComponent.createDirectionCommand(Direction.RIGHT, distance);
        randComponent.processInput();
        balloom.getPhysics().move(SECOND_TO_MILLISECOND);
        Assert.assertEquals(balloom.getPosition(),
                                  new Position(previousPos.getX() + balloom.getVelocity() * GAME_SCALE, previousPos.getY()));
    }

    /**
     * Test interactions enemies - environment.
     *
     * @throws LineUnavailableException 
     * @throws IOException 
     * @throws UnsupportedAudioFileException 
     */
    @Test
    public void testEnemyCollision() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        //World initialization
        final Theme theme = new ThemeImpl("/ZeldaTheme");
        final World world = new WorldImpl(theme, null);
        world.getGraphicComponent().dispose();
        //Game Objects initialization
        final Bomb bomb = new Bomb(new Position(BOMB_X_COORDINATE * GAME_SCALE,
                                                BOMB_Y_COORDINATE * GAME_SCALE),
                                   null, world);
        final Block safeBlock = world.getBlocks()
                                     .stream()
                                     .filter(p -> p.getPosition()
                                                   .equals(new Position(SAFE_BLOCK_X_COORDINATE * GAME_SCALE,
                                                                       SAFE_BLOCK_Y_COORDINATE * GAME_SCALE)))
                                     .findFirst()
                                     .get();
        //Enemies creation
        final GhostEnemy ghostEnemy = new GhostEnemy(
                new Position(GHOST_ENEMY_START_X_COORDINATE * GAME_SCALE, GHOST_ENEMY_START_Y_COORDINATE * GAME_SCALE),
                             null, null, null);
        final SmartEnemy smartEnemy = new SmartEnemy(
                new Position(SMART_ENEMY_START_X_COORDINATE * GAME_SCALE, SMART_ENEMY_START_Y_COORDINATE * GAME_SCALE),
                             null, null, null);
        final BalloomEnemy balloomEnemy = new BalloomEnemy(new Position(BALLOOM_ENEMY_START_X_COORDINATE * GAME_SCALE,
                                                                        BALLOOM_ENEMY_START_Y_COORDINATE * GAME_SCALE),
                                                                        null, null, null);
        // Collision test
        Assert.assertTrue(ghostEnemy.getPhysics().checkGenericCollision(world.getPlayer()));
        ghostEnemy.getPosition().setX(GHOST_ENEMY_NEW_X_COORDINATE * GAME_SCALE);
        Assert.assertFalse(ghostEnemy.getPhysics().checkGenericCollision(world.getPlayer()));
        Assert.assertFalse(smartEnemy.getPhysics().checkGenericCollision(safeBlock));
        Assert.assertFalse(smartEnemy.getPhysics().checkGenericCollision(bomb));
        Assert.assertTrue(balloomEnemy.getPhysics().checkGenericCollision(bomb));
        smartEnemy.getPosition().setX(SMART_ENEMY_NEW_X_COORDINATE * GAME_SCALE);
        smartEnemy.getPosition().setY(SMART_ENEMY_NEW_Y_COORDINATE * GAME_SCALE);
        Assert.assertTrue(smartEnemy.getPhysics().checkGenericCollision(bomb));
        balloomEnemy.getPosition().setY(BALLOOM_ENEMY_NEW_Y_COORDINATE * GAME_SCALE);
        Assert.assertTrue(balloomEnemy.getPhysics().checkGenericCollision(safeBlock));
    }
}
