package menu.frame;

import java.awt.BorderLayout;
import java.net.URL;

import javax.swing.JPanel;

import game.state.GameContext;
import menu.frame.components.GameImgIcon;
import menu.frame.components.GamePanelImpl;
import menu.frame.components.InfoLabel;
import menu.frame.themebuttons.BackButtonImpl;

/**
 * Windows that shows game controls and a little game description.
 */
public class InfoMenu extends AbstractGameFrame {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final String IMAGE_PATH = "MenuComponents/Images/InfoMenu/";
    private static final URL IMG_URL = ClassLoader.getSystemResource(IMAGE_PATH + "info.png");
    private static final double IMAGE_SCALE = 0.6;

    /**
     *  Creates {@code InfoMenu} window with back button on bottom,
     *  {@link InfoLabel} on top, and a little game description in the center.
     *
     *  @param gameContext the {@link GameContext} handler.
     */
    public InfoMenu(final GameContext gameContext) {
        super(new BorderLayout());
        final JPanel north = new GamePanelImpl();
        final JPanel center = new GamePanelImpl(new GameImgIcon(IMG_URL, IMAGE_SCALE));
        final JPanel south = new GamePanelImpl();
        north.add(new InfoLabel());
        south.add(new BackButtonImpl(gameContext));
        this.getContentPane().add(north, BorderLayout.NORTH);
        this.getContentPane().add(center, BorderLayout.CENTER);
        this.getContentPane().add(south, BorderLayout.SOUTH);
        this.setResizable(false);
        this.pack();
        super.setLocationRelativeTo(null);
    }

}
