package menu.frame.components;

import java.net.URL;

import javax.swing.Icon;
import javax.swing.JLabel;

import menu.frame.buttons.AbstractGameButton;

/**
 * This class provides a skeletal implementation of game label and should be
 * implemented to standardize the game design. In this specified case, it's a
 * display area for a externally loaded image ({@link GameImgIcon}).
 */
public abstract class AbstractGameLabel extends JLabel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates {@code AbstractGameLabel} with no image and with an empty
     * string for the title.
     */
    public AbstractGameLabel() {
        super();
        super.setForeground(AbstractGameButton.TEXT_BUTTON_COLOR);
        super.setFont(new GameFont().getDefaultGameFont());
    }

    /**
     * Creates {@code AbstractGameLabel} with string for the title.
     * 
     * @param title the text to be load
     */
    public AbstractGameLabel(final String title) {
        this();
        super.setText(title);
    }

    /**
     * Creates {@code AbstractGameLabel} with {@link Icon}.
     * 
     * @param icon the {@link Icon} image to be load.
     */
    public AbstractGameLabel(final Icon icon) {
        this();
        super.setIcon(icon);
    }

    /**
     * Creates {@code AbstractGameLabel} with {@link GameImgIcon} loaded from
     * the specified {@link URL} and resized using a specified real scalar.
     * 
     * @param imgURL the {@link URL} pointer to get image "resource"
     * @param scale  the real scalar value to resize the image
     */
    public AbstractGameLabel(final URL imgURL, final double scale) {
        this();
        super.setIcon(new GameImgIcon(imgURL, scale));
    }

}
