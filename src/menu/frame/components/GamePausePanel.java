package menu.frame.components;

import java.awt.LayoutManager;

/**
 * This class models the Panel component of the Game Pause Menu.
 */
public class GamePausePanel extends AbstractGamePanel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates {@code GamePausePanel}.
     *
     * @param layoutManager of the panel
     */
    public GamePausePanel(final LayoutManager layoutManager) {
        super(layoutManager);
        this.setOpaque(false);
    }

}
