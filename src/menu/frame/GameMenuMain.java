package menu.frame;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JPanel;

import game.state.GameContext;
import menu.frame.buttons.ExitButton;
import menu.frame.buttons.InfoButton;
import menu.frame.buttons.NewGameButton;
import menu.frame.components.GameImgIcon;
import menu.frame.components.GameLabelImpl;
import menu.frame.components.GamePanelImpl;

/**
 * This is the initial game menu where you can choose different choices
 * including new game, options and exits. This class is designed to
 * automatically scale down based of the main screen size. It implements
 * specific game components that make standard the overall design. For further
 * information see {@link AbstractGameFrame}, {@link GamePanelImpl},
 * {@link GameLabelImpl}, {@link NewGameButton}, {@link InfoButton} and
 * {@link ExitButton} classes.
 */
public class GameMenuMain extends AbstractGameFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static final int DISTANCE_BUTTON = 20;
    private static final String IMAGE_PATH = "MenuComponents/Images/MainMenuBackground/";
    private static final URL IMG_URL_WEST = ClassLoader.getSystemResource(IMAGE_PATH + "MM_Background_West.png");
    private static final URL IMG_URL_EAST = ClassLoader.getSystemResource(IMAGE_PATH + "MM_Background_East.png");
    private static final double SCALE_IMG_WEST = 0.7;
    private static final double SCALE_IMG_EAST = 0.3;

    /**
     * Creates a {@code GameMenuMain} frame with default settings.
     * 
     * @param gameContext the {@link GameContext} handler
     */
    public GameMenuMain(final GameContext gameContext) {
        super(new BorderLayout());
        final JPanel eastPanel = new GamePanelImpl(new BorderLayout());
        final JPanel westPanel = new GamePanelImpl(new GameImgIcon(IMG_URL_WEST, SCALE_IMG_WEST));
        final JPanel internalJpanel = new GamePanelImpl(new GridBagLayout());
        drawButtonInPanel(gameContext, internalJpanel);
        eastPanel.add(new GameLabelImpl(IMG_URL_EAST, SCALE_IMG_EAST), BorderLayout.NORTH);
        eastPanel.add(internalJpanel, BorderLayout.CENTER);
        super.getContentPane().add(westPanel, BorderLayout.WEST);
        super.getContentPane().add(new GamePanelImpl(), BorderLayout.CENTER);
        super.getContentPane().add(eastPanel, BorderLayout.EAST);
        super.pack();
        super.setResizable(false);
        super.setLocationRelativeTo(null);
    }

    private void drawButtonInPanel(final GameContext gameContext, final JPanel panel) {
        final GridBagConstraints cnst = new GridBagConstraints();
        final JPanel northPanel = new GamePanelImpl();
        final JPanel centerPanel = new GamePanelImpl();
        final JPanel southPanel = new GamePanelImpl();
        final JButton startGame = new NewGameButton(gameContext);
        final JButton optionGame = new InfoButton(gameContext);
        final JButton exitGame = new ExitButton();
        northPanel.add(startGame);
        centerPanel.add(optionGame);
        southPanel.add(exitGame);
        cnst.insets = new Insets(DISTANCE_BUTTON, DISTANCE_BUTTON, DISTANCE_BUTTON, DISTANCE_BUTTON);
        cnst.gridy = 0;
        panel.add(northPanel, cnst);
        cnst.gridy++;
        panel.add(centerPanel, cnst);
        cnst.gridy++;
        panel.add(southPanel, cnst);
    }

}
