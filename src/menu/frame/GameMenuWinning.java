package menu.frame;

import java.awt.Image;
import java.net.URL;

import game.state.GameContext;
import menu.frame.components.GameImgIcon;

/**
 * This is the game over menu where you can choose two different choices: main
 * menu or exits. This class is designed to automatically scale down based of
 * the main screen size. It implements specific game components that make
 * standard the overall design.
 */
public class GameMenuWinning extends AbstractGameMenu {

    private static final long serialVersionUID = 1L;
    private static final String IMAGE_PATH = "MenuComponents/Images/GameWinningMenuBackground/";
    private static final URL IMAGE_URL_WINNING = ClassLoader.getSystemResource(IMAGE_PATH + "Winning.gif");
    private static final double IMG_SCALE = 0.6;

    /**
     * Creates a {@code GameMenuWinning}.
     * 
     * @param gameContext the {@link GameContext} handler.
     */
    public GameMenuWinning(final GameContext gameContext) {
        super(gameContext, new GameImgIcon(IMAGE_URL_WINNING, IMG_SCALE, Image.SCALE_DEFAULT));
    }

}
