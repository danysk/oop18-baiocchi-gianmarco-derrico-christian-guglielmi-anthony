package menu.frame.buttons;

import game.state.GameContext;

/**
 * This class extends {@link AbstractGameButton}. It allows easy management of
 * the commons behaviors and settings of the MainMenu button in case of one or
 * multiple uses within its own program.
 */
public class MainMenuButton extends AbstractGameButton {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static final String TEXT_BUTTON = "MAIN MENU";

    /**
     * Creates a {@code MainMenuButton}.
     * 
     * @param gameContext the {@link GameContext} handler
     */
    public MainMenuButton(final GameContext gameContext) {
        super(TEXT_BUTTON);
        super.addActionListener(l -> gameContext.requestMainMenuState());
    }

}
