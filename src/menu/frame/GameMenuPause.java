/**
 *
 */
package menu.frame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Optional;

import javax.swing.JButton;
import javax.swing.JPanel;

import game.state.GameContext;
import menu.frame.buttons.ExitButton;
import menu.frame.buttons.ResumeButton;
import menu.frame.components.GamePausePanel;
import menu.frame.components.PauseMenuLabel;

/**
 * This class models the Game Menu Pause.
 */
public class GameMenuPause extends AbstractGameFrame {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final int DISTANCE_BUTTON = 40;
    private static final float OPACITY = 0.7f;
    private static final int SCALE = 3;
    private static final int HEIGHT = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight() / SCALE;
    private static final int WIDTH = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth() / SCALE;

     /**
     * Creates {@code GameMenuPause}.
     *
     *  @param context game-state controller
     */
    public GameMenuPause(final GameContext context) {
        super(new BorderLayout());
        this.setVisible(false);
        this.setUndecorated(true);
        this.setOpacity(OPACITY);
        final JPanel centerPanel = new GamePausePanel(new BorderLayout());
        final JPanel westInternalCenterPanel = new GamePausePanel(new GridBagLayout());
        final JPanel eastInternalcenterPanel = new GamePausePanel(new GridBagLayout());
        final JPanel northPanel = new GamePausePanel(new GridBagLayout());
        northPanel.add(new PauseMenuLabel());
        final JButton exit = new ExitButton();
        exit.setForeground(Color.black);
        final JButton resume = new ResumeButton(context);
        drawComponentInPanel(westInternalCenterPanel, resume, DISTANCE_BUTTON);
        drawComponentInPanel(eastInternalcenterPanel, exit, DISTANCE_BUTTON);
        centerPanel.add(westInternalCenterPanel, BorderLayout.WEST);
        centerPanel.add(eastInternalcenterPanel, BorderLayout.EAST);
        this.getContentPane().add(northPanel, BorderLayout.CENTER);
        this.getContentPane().add(centerPanel, BorderLayout.SOUTH);
        this.setSize(new Dimension(WIDTH, HEIGHT));
        this.setLocationRelativeTo(null);
        this.addKeyListener(new KeyListener() {

            @Override
            public void keyPressed(final KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_P) {
                    context.requestRunningState(Optional.empty());
                }
            }

            @Override
            public void keyTyped(final KeyEvent e) {
            }

            @Override
            public void keyReleased(final KeyEvent e) {
            }
        });
    }

}
