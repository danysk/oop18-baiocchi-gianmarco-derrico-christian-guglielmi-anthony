package menu.frame;

import java.awt.BorderLayout;
import java.awt.Canvas;

import game.world.World;
import menu.frame.components.GameInfoRunningPanel;
import menu.frame.components.WorldCanvas;
import menu.frame.components.WorldCanvasImpl;

/**
 * This class is the JFrame of running state.
 */
public class WorldGraphicComponentImpl extends AbstractGameFrame {

    private static final long serialVersionUID = 1L;
    private final WorldCanvas canvas;
    private final GameInfoRunningPanel panel;

    /**
     * Creates {@code WorldGraphicComponentImpl}.
     * 
     * @param world the link to {@link World}
     */
    public WorldGraphicComponentImpl(final World world) {
        super();
        this.canvas = new WorldCanvasImpl(world, world.getTheme());
        this.panel = new GameInfoRunningPanel(world);
        this.disposeComponent();
    }

    /**
     * Renders all world's objects.
     */
    public void render() {
        this.canvas.render();
        this.panel.render();
    }

    private void disposeComponent() {
        super.setLayout(new BorderLayout());
        if (this.canvas instanceof Canvas) {
        super.getContentPane().add((Canvas) this.canvas, BorderLayout.CENTER);
        } else {
            throw new ClassCastException();
        }
        super.getContentPane().add(this.panel, BorderLayout.NORTH);
        super.setResizable(false);
        super.setVisible(true);
        super.pack();
        super.setLocationRelativeTo(null);
        ((Canvas) this.canvas).requestFocus();
    }

}
