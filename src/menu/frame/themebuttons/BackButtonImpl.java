package menu.frame.themebuttons;

import game.state.GameContext;
import menu.frame.buttons.MainMenuButton;

/**
 * It represents a back button to return to the main menu from theme menu.
 */
public class BackButtonImpl extends MainMenuButton {

    /**
     *
     */
    private static final long serialVersionUID = 5158587578483224358L;

    /**
     * Creates a {@code BackButtonImpl} to back in the main menu window.
     *
     * @param gameContext to get the main menu state
     */
    public BackButtonImpl(final GameContext gameContext) {
        super(gameContext);
        this.setText("BACK");
    }

}
