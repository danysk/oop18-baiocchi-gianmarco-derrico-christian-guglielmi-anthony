package menu.frame.themebuttons;

import java.awt.Image;
import java.util.Optional;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

import game.state.GameContext;

/**
 * This class is the button to choose the kingdom hearts theme in game and extends {@link AbstractThemeMenuButton}.
 */
public class SoraButtonImpl extends AbstractThemeMenuButton {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final int BUTTON_PROPORTION_SIZE = 3;
    private static final String FOLDER = "/ChooseThemeMenu";
    private static final String SORA_FOLDER_THEME = "/KingdomHeartsTheme";

    private final GameContext gameContext;

    /**
     * Creates {@code SoraButtonImpl}.
     *
     * @param frame to close when button is pressed
     * @param gameContext the game context handler.
     */
    public SoraButtonImpl(final JFrame frame, final GameContext gameContext) {
        super();
        this.gameContext = gameContext;
        final int buttonSize = frame.getWidth() / BUTTON_PROPORTION_SIZE;
        final ImageIcon sora = new ImageIcon(new ImageIcon(
                               this.getClass().getResource(FOLDER + "/sora.png"))
                                              .getImage().getScaledInstance(buttonSize,
                                                                            buttonSize,
                                                                            Image.SCALE_SMOOTH));
        final ImageIcon kh = new ImageIcon(new ImageIcon(
                               this.getClass().getResource(FOLDER + "/kingdomHearts.png"))
                                              .getImage().getScaledInstance(buttonSize,
                                                                            buttonSize,
                                                                            Image.SCALE_SMOOTH));
        this.setIcon(sora);
        this.loadMouseListener(kh);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void chooseTheme() {
        this.gameContext.requestRunningState(Optional.of(SORA_FOLDER_THEME));
    }

}
