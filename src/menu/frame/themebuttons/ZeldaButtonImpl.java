package menu.frame.themebuttons;

import java.awt.Image;
import java.util.Optional;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

import game.state.GameContext;

/**
 * This class is the button to choose the Zelda theme in game and extends {@link AbstractThemeMenuButton}.
 */
public class ZeldaButtonImpl extends AbstractThemeMenuButton {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final int BUTTON_PROPORTION_SIZE = 3;
    private static final String FOLDER = "/ChooseThemeMenu";
    private static final String ZELDA_FOLDER_THEME = "/ZeldaTheme";

    private final GameContext gameContext;

    /**
     * Creates {@code ZeldaButtonImpl}.
     *
     * @param frame to close when button is pressed
     * @param  gameContext the game context handler.
     */
    public ZeldaButtonImpl(final JFrame frame, final GameContext gameContext) {
        super();
        this.gameContext = gameContext;
        final int buttonSize = frame.getWidth() / BUTTON_PROPORTION_SIZE;
        final ImageIcon link = new ImageIcon(new ImageIcon(
                               this.getClass().getResource(FOLDER + "/link.png"))
                                              .getImage().getScaledInstance(buttonSize,
                                                                            buttonSize,
                                                                            Image.SCALE_SMOOTH));
        final ImageIcon zelda = new ImageIcon(new ImageIcon(
                                this.getClass().getResource(FOLDER + "/zelda.png"))
                                               .getImage().getScaledInstance(buttonSize,
                                                                             buttonSize,
                                                                             Image.SCALE_SMOOTH));
        this.setIcon(link);
        this.loadMouseListener(zelda);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void chooseTheme() {
        this.gameContext.requestRunningState(Optional.of(ZELDA_FOLDER_THEME));
    }

}
