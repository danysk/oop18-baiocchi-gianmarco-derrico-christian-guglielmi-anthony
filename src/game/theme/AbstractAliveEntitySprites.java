package game.theme;

import java.util.ArrayList;
import java.util.List;

import game.graphics.Sprite;

/**
 * Class to store any {@link Sprite} about alive entities like bomb or flame.
 */
public abstract class AbstractAliveEntitySprites {

    private final List<Sprite> sprites;

    /**
     * Creates an {@code AbstractFlameBombSprites}.
     */
    public AbstractAliveEntitySprites() {
        this.sprites = new ArrayList<>();
    }

    /**
     * Gets the sprite about the single animation.
     *
     * @return a list of {@link Sprite}
     */
    public List<Sprite> getSprites() {
        return this.sprites;
    }

    /**
     * Gets the number of sprites for the animation.
     *
     * @return the size of the {@link Sprite} list
     */
    public abstract int getSpritesNumber();

}

