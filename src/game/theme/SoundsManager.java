package game.theme;

import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import sounds.Sound;
import sounds.SoundImpl;

/**
 * This class stores and gets any {@link Sound}.
 */
public class SoundsManager {

    private Sound explosion;
    private Sound music;
    private Sound door;

    /**
     * Creates a {@code SoundsManager}.
     *
     * @param folder where to take the sounds
     * @throws UnsupportedAudioFileException : wrong audio file format
     * @throws IOException : problem during input/output
     * @throws LineUnavailableException : audio line can't be opened because it is unavailable
     */
    public SoundsManager(final String folder) {
        try {
            this.explosion = new SoundImpl("/explosion.wav");
            this.music = new SoundImpl(folder + "/music.wav");
            this.door = new SoundImpl("/door.wav");
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the music in game.
     *
     * @return music {@link Sound}
     */
    public Sound getMusicSound() {
        return this.music;
    }

    /**
     * Gets the opening door sound.
     *
     * @return opening door {@link Sound}
     */
    public Sound getDoorSound() {
        return this.door;
    }

    /**
     * Gets the explosion sound.
     *
     * @return explosion {@link Sound}
     */
    public Sound getExplosionSound() {
        return this.explosion;
    }

}
