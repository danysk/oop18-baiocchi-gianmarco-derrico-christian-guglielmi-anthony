package game.physics;

import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

import game.graphics.SpriteSheet;
import game.object.GameObject;
import game.object.Solidity;
import game.objects.blocks.Bomb;
import game.objects.blocks.Flame;
import game.theme.FlameSprites;
import game.utilities.Position;
import game.world.World;

/**
 * Class to manage {@link BombPhysicsComponent}.
 */
public class BombPhysicsComponent extends AbstractPhysicsComponent {

    private final BlockingQueue<Flame> flames;
    private final Bomb bomb;

    /**
     * Creates a {@code BombPhysicsComponent}.
     *
     * @param bomb the {@link Bomb} where adding a physic
     */
    public BombPhysicsComponent(final Bomb bomb) {
        super(bomb);
        this.bomb = bomb;
        this.flames = new LinkedBlockingQueue<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void checksCollisions(final Set<? extends GameObject> object) {
    }

    /**
     * Generates the {@link Bomb} explosion.
     *
     * @param world the game {@link World}
     */
    public void explosion(final World world) {
        final FlameSprites flameSprite = world.getTheme().getSprites().getFlame();
        final double xPosition = this.bomb.getPosition().getX();
        final double yPosition = this.bomb.getPosition().getY();
        createCenterFlame(new Position(bomb.getPosition()), flameSprite); // center
        createSideFlame(new Position(xPosition + SpriteSheet.SPRITE_SIZE_IN_GAME, yPosition), flameSprite, world); // TP
        createSideFlame(new Position(xPosition - SpriteSheet.SPRITE_SIZE_IN_GAME, yPosition), flameSprite, world); // DW
        createSideFlame(new Position(xPosition, yPosition + SpriteSheet.SPRITE_SIZE_IN_GAME), flameSprite, world); // RT
        createSideFlame(new Position(xPosition, yPosition - SpriteSheet.SPRITE_SIZE_IN_GAME), flameSprite, world); // LT
        world.addFlames(this.flames);
        world.removeObject(this.bomb);
    }

    private void createCenterFlame(final Position bombPosition, final FlameSprites flameSprite) {
        this.flames.add(new Flame(bombPosition, flameSprite, this.bomb));
    }

    private void createSideFlame(final Position bombPosition, final FlameSprites flameSprite, final World world) {
        if (isTileFree(bombPosition, world) || isTileBreakable(bombPosition, world)) {
            this.flames.add(new Flame(bombPosition, flameSprite, this.bomb));
        }
    }

    private boolean isTileFree(final Position flamePosition, final World world) {
        return world.getFreePosition().contains(flamePosition);
    }

    private boolean isTileBreakable(final Position flamePosition, final World world) {
        final Set<Position> breakableBlocks = world.getBlocks().stream()
                                                                .filter(breakable -> breakable.isBreakable()
                                                                                        .equals(Solidity.BREAKABLE))
                                                                .map(block -> block.getPosition())
                                                                .collect(Collectors.toSet());
        return breakableBlocks.contains(flamePosition);
    }

}
