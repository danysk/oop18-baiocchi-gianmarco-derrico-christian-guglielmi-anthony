package game.physics;

/**
 * Enum to define the entity way.
 */
public enum Direction {

    /**
     * Right way.
     */
    RIGHT,

    /**
     * Left way.
     */
    LEFT,

    /**
     * Up way.
     */
    UP,

    /**
     * Down way.
     */
    DOWN,

    /**
     * Motionless status.
     */
    STOP;
}
