package game.maps;

import java.util.stream.Collectors;

import game.graphics.SpriteSheet;
import game.world.GameObjectFactory;

/**
 * It represents a normal {@link GameMap} layout where blocks are evenly distributed.
 */
public class NormalMap extends AbstractGameMap {

    private static final int SPRITE_SIZE = SpriteSheet.SPRITE_SIZE_IN_GAME;
    private static final int EVEN_INDEX = 2;

    /**
     * Creates a {@code NormalMap} composed by blocks with same distance between them.
     *
     * @param factory {@link GameObjectFactory} to create blocks
     */
    public NormalMap(final GameObjectFactory factory) {
        super(factory);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initializeMapLayout(final GameObjectFactory factory) {
        this.getMapLayout().addAll(super.getFreePositions().stream()
                           .filter(p -> (p.getX() / SPRITE_SIZE) % EVEN_INDEX == 0
                                         && (p.getY() / SPRITE_SIZE) % EVEN_INDEX == 0)
                           .map(a -> factory.createUnbreakableBlock(a))
                           .collect(Collectors.toSet()));
    }

}
