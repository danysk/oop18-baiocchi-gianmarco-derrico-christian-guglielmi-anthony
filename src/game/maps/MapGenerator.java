package game.maps;

/**
 * Class that represents a game map supplied casually.
 */
public interface MapGenerator {

    /**
     * Gets a set of unbreakable blocks.
     *
     * @return a map still {@link block} layout generated casually
     */
    GameMap getMapType();

}
