/**
 * 
 */
package game.utilities;

/**
 * The player score manager to keep a game score up to date.
 */
public class PlayerScoreImpl implements PlayerScore {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static final int INITIAL_SCORE = 0;

    private int score;

    /**
     * Creates a {@code PlayerScoreImpl} manager.
     */
    public PlayerScoreImpl() {
        super();
        this.score = INITIAL_SCORE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getScore() {
        return this.score;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addScore(final int score) {
        this.score = this.score + score;
    }

}
