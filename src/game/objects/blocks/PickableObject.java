package game.objects.blocks;

import java.awt.Graphics2D;

import game.graphics.GraphicsComponent;
import game.graphics.Sprite;
import game.graphics.StillObjectGraphicComponent;
import game.object.Solidity;
import game.utilities.Position;

/**
 * This class manages all the objects that could be taken by the player.
 * It extends {@link AbstractStillObject}.
 */
public class PickableObject extends AbstractStillObject {

    private final PowerUpAction action;
    private final GraphicsComponent graphicComponent;

    /**
     * Creates a {@code PickableObject}.
     * 
     * @param position object's position
     * @param action the propriety of the object
     * @param sprite the sprite of the object
     */
    public PickableObject(final Position position, final PowerUpAction action,
            final Sprite sprite) {
        super(Solidity.UNBREAKABLE, position);
        this.action = action;
        this.graphicComponent = new StillObjectGraphicComponent(this, sprite);
    }

    /**
     * Gets for the propriety action of the object.
     * 
     * @return pickable object's propriety action
     */
    public PowerUpAction getAction() {
        return this.action;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final Graphics2D g) {
        this.graphicComponent.render(g);
    }

}
