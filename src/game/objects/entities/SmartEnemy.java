package game.objects.entities;

import java.awt.Graphics2D;

import game.graphics.DynamicEntityGraphicsComponent;
import game.graphics.GraphicsComponent;
import game.input.EnemyInputComponent;
import game.input.SmartInputComponent;
import game.object.Solidity;
import game.theme.EnemySprites;
import game.utilities.Position;
import game.world.World;

/**
 * This class models the SmartEnemy.
 * It follows the {@link PlayerImpl}, if there's a path between them.
 */
public class SmartEnemy extends AbstractEnemy {

    private static final double SMART_TILE_PER_SECOND = 1.5;
    private static final int SMART_ENEMY_SCORE = 25;
    private final EnemyInputComponent smartInputComponent;
    private final GraphicsComponent gfxBalloomComponent;

    /**
     * Creates {@code SmartEnemy}.
     *
     * @param position {@link Position} of Smart Enemy
     * @param breakable {@link Solidity} of Smart Enemy
     * @param world that contains Smart Enemy
     * @param sprite to draw Smart Enemy
     */
    public SmartEnemy(final Position position, final Solidity breakable, final World world, final EnemySprites sprite) {
        super(position, breakable, world);
        this.smartInputComponent = new SmartInputComponent(this, SMART_TILE_PER_SECOND);
        this.gfxBalloomComponent = new DynamicEntityGraphicsComponent(this, sprite);
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void update(final long elapsedTime) {
        super.update(elapsedTime);
        super.checkPhysicalCollision();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void processInput() {
        this.generateCommand();
        this.smartInputComponent.processInput();
    }

    private void generateCommand() {
        if (super.isInTileCenter()) {
            this.smartInputComponent.generateCommand();
        }
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void render(final Graphics2D g) {
        this.gfxBalloomComponent.render(g);
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public int getEnemyScore() {
        return SMART_ENEMY_SCORE;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public double getVelocity() {
        return SMART_TILE_PER_SECOND;
    }

}
