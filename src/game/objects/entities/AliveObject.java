package game.objects.entities;

import game.object.GameObject;

/**
 * Interface for any "alive object" in game (bomb includes).
 */
public interface AliveObject extends GameObject {

    /**
     * Updates entity state.
     * 
     * @param elapsedTime between two consecutive frames
     */
    void update(long elapsedTime);
}
