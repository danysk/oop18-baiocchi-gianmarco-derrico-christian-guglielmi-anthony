package game.objects.entities;

import game.physics.DynamicPhysicsComponent;
import game.world.World;

/**
 *  Interface for moving objects.
 */
public interface DynamicObject extends AliveObject {

    /**
     * Process input entity.
     */
    void processInput();

    /**
     * Gets the entity's physics.
     * 
     * @return a physics component.
     */
    DynamicPhysicsComponent getPhysics();

    /**
     * Gets the world.
     * 
     * @return world that contains {@code AbstractEntity}
     */
    World getWorld();

}
