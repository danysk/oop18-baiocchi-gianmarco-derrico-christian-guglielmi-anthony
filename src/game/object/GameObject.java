package game.object;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import game.utilities.Position;

/**
 * This interface declares all method of all objects in the game.
 */
public interface GameObject {

    /**
     * Gets for the position of the object.
     * 
     * @return object's position
     */
    Position getPosition();

    /**
     * Gets for the rectangle of the body.
     * 
     * @return body's rectangle
     */
    Rectangle2D getBounds();

    /**
     * Checks if the object is breakable.
     * 
     * @return true if the object is breakable, otherwise false
     */
    Solidity isBreakable();

    /**
     * Renders object's sprite.
     * 
     * @param g component that draw the sprite
     */
    void render(Graphics2D g);
}
