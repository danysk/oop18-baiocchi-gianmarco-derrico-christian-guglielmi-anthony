package game.state;

import javax.swing.JFrame;

import menu.frame.GameMenuMain;

/**
 * The {@link GameState} that's run the {@link GameMenuMain}.
 */
public class MainMenuState implements GameState {

    private final JFrame mainMenu;

    /**
     * Creates a {@code MainMenuState}.
     * 
     * @param gameContext the {@link GameContext} handler.
     */
    public MainMenuState(final GameContext gameContext) {
        this.mainMenu = new GameMenuMain(gameContext);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void runState() {
        this.mainMenu.setVisible(true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void closeFrameState() {
        this.mainMenu.dispose();
    }

}
