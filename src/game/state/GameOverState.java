package game.state;

import javax.swing.JFrame;

import menu.frame.GameMenuGameOver;

/***
 * The {@link GameState} that's run the {@link GameMenuGameOver}.
 */
public class GameOverState implements GameState {

    private final JFrame gameOver;

    /**
     * Creates a {@code GameOverState}.
     * 
     * @param gameContext the {@link GameContext} handler.
     */
    public GameOverState(final GameContext gameContext) {
        this.gameOver = new GameMenuGameOver(gameContext);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void runState() {
        this.gameOver.setVisible(true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void closeFrameState() {
        this.gameOver.dispose();
    }

}
