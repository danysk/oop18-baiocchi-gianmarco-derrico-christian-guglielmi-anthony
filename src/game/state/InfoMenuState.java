package game.state;

import javax.swing.JFrame;

import menu.frame.InfoMenu;

/**
 * The {@link GameState} that that's run the {@link InfoMenu}.
 */
public class InfoMenuState implements GameState {

    private final JFrame info;

    /**
     * Creates the {@code InfoMenuState}.
     * 
     * @param gameContext the {@link GameContext} handler.
     */
    public InfoMenuState(final GameContext gameContext) {
        super();
        this.info = new InfoMenu(gameContext);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void runState() {
        this.info.setVisible(true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void closeFrameState() {
        this.info.dispose();
    }

}
