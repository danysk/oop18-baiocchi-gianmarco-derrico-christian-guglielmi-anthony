package game.state;

import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import game.engine.GameEngine;
import game.engine.GameEngineImpl;
import game.theme.Theme;
import game.theme.ThemeImpl;

/**
 * This class manages the running state of the game.
 * It models {@link GameState}.
 */
public class RunningState implements GameState {

    private Theme theme;
    private final GameEngine controller;

    /**
     * Creates a {@code RunningState}.
     *
     * @param gameContext the {@link GameContext} handler.
     * @param folder the directory where there is the theme
     */
    public RunningState(final GameContext gameContext, final String folder) {
        this.theme = loadTheme(folder);
        this.controller = new GameEngineImpl(this.theme, gameContext);
        this.controller.startLoop();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void runState() {
        this.controller.setUpdatable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void closeFrameState() {
        this.theme.getSounds().getMusicSound().stop();
        this.controller.unsetUpdatable();
    }

    /**
     * Stops the game cycle.
     */
    protected void stopLoop() {
        this.theme.getSounds().getMusicSound().stop();
        this.controller.stopLoop();
    }

    private Theme loadTheme(final String folder) {
        try {
            theme = new ThemeImpl(folder);
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            e.printStackTrace();
        }
        return theme;
    }
}
