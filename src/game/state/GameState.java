package game.state;

/**
 * It models the state for any {@link GameState}.
 */
public interface GameState {

    /**
     * Throws the implemented state.
     */
    void runState();

    /**
     * Closes the implemented state.
     */
    void closeFrameState();

}
