package game.graphics;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import game.engine.GameEngineImpl;
import game.objects.entities.AliveObject;
import game.theme.AbstractAliveEntitySprites;

/**
 * This class manages any alive object graphics component in the game.
 */
public class AliveEntityGraphicsComponent implements GraphicsComponent {

    private static final int DIVISION_BY_ZERO_PROTECTION = 1;
    private static final int FRAME_DELAY = Math.round(GameEngineImpl.FPS / 15) + DIVISION_BY_ZERO_PROTECTION;

    private final AliveObject object;
    private final AbstractAliveEntitySprites sprites;
    private int frame;
    private int updateFrame;

    /**
     * Creates {@code FlameBombGraphicsComponent}.
     *
     * @param sprites to animate an alive object
     * @param object reference to draw it with its {@link Sprite}
     */
    public AliveEntityGraphicsComponent(final AbstractAliveEntitySprites sprites, final AliveObject object) {
        this.frame = 0;
        this.updateFrame = 0;
        this.object = object;
        this.sprites = sprites;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final Graphics2D g) {
        this.nextFrame();
        g.drawImage(this.sprites.getSprites().get(this.frame).getImage(),
                AffineTransform.getTranslateInstance(this.object.getPosition().getX(),
                                                     this.object.getPosition().getY()), null);
    }

    private void nextFrame() {
        this.updateFrame++;
        if (this.updateFrame % FRAME_DELAY == 0) {
            this.frame++;
            this.updateFrame = 0;
            this.frame = this.frame >= this.sprites.getSpritesNumber() ? 0 : this.frame;
        }
    }

}
