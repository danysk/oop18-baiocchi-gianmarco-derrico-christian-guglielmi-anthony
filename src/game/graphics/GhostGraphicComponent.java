package game.graphics;

import java.awt.Graphics2D;
import java.util.Set;
import java.util.stream.Collectors;

import game.object.GameObject;
import game.objects.entities.AbstractEntity;
import game.theme.AbstractDynamicEntitySprites;
import game.utilities.Position;

/**
 * This class realizes the {@link GraphicsComponent} for Ghost Enemy.
 */
public class GhostGraphicComponent extends DynamicEntityGraphicsComponent {

    /**
     * Creates {@code GhostGraphicComponent}.
     *
     * @param entity GhostEnemy to draw
     * @param sprites Ghost {@link Sprite} images
     */
    public GhostGraphicComponent(final AbstractEntity entity, final AbstractDynamicEntitySprites sprites) {
        super(entity, sprites);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final Graphics2D graphics) {
        final Set<? extends GameObject> obj = super.getEntity().getWorld().getAllGameObjects()
                                                                          .stream()
                                                                          .collect(Collectors.toSet());
        if (!obj.stream()
               .filter(o -> super.getEntity().getWorld().getBombs().contains(o)
                       || super.getEntity().getWorld().getBlocks().contains(o))
               .map(m -> m.getPosition())
               .anyMatch(p -> p.equals(new Position(super.getEntity()
                                                         .getPosition()
                                                         .setInTile(super.getEntity().getPosition()))))) {
            super.render(graphics);
        }
    }

}
