package game.input;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import game.graphics.SpriteSheet;
import game.input.graph.Graph;
import game.input.graph.GraphImpl;
import game.objects.entities.Enemy;
import game.physics.Direction;
import game.utilities.Pair;
import game.utilities.Position;

/**
 * This class models a {@link EnemyInputComponent}.
 * It determines the SmartEnemy's behavior: a path of {@link Direction} is calculated
 * and used in order to guide the SmartEnemy to the player.
 */
public class SmartInputComponent extends AbstractInputComponent implements EnemyInputComponent {

    private final double pixelOnSecond;

    private int difficultyLevel;
    private int tileNumber;
    private Graph graph;
    private final Enemy enemy;

    /**
     * Builds {@code SmartInputComponent}.
     *
     * @param entity {@link Enemy}.
     * @param distance covered by the enemy in a second.
     */
    public SmartInputComponent(final Enemy entity, final double distance) {
        super(entity);
        this.pixelOnSecond = distance;
        this.enemy = entity;
        this.difficultyLevel = 1;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public int getDifficultyLevel() {
        return this.difficultyLevel;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void incrementDifficultyLevel() {
        this.difficultyLevel += 1;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void generateCommand() {
        List<Direction> list = new ArrayList<>();
        final int tileNumber = this.enemy.getWorld().getFreePosition().size();
        if (this.tileNumber < tileNumber) {
            this.tileNumber = tileNumber;
            this.createGraph();
        }
        list = this.graph.search(this.enemy.getPosition(), this.enemy.getWorld()
                                                               .getPlayer()
                                                               .getPosition());
        if (!list.isEmpty()) {
            super.createDirectionCommand(list.remove(0), (this.pixelOnSecond * SpriteSheet.SPRITE_SIZE_IN_GAME) * this.difficultyLevel);
        } else {
            super.createDirectionCommand(Direction.STOP, 0);
        }
    }

    private void createGraph() {
        this.graph = new GraphImpl();
        final Set<Position> freePos = this.enemy.getWorld().getFreePosition();
        freePos.forEach(p -> {
            final Position pos = new Position(p);
            if (!this.graph.containsNode(pos)) {
                this.graph.addNode(pos);
            }
            this.addEdges(pos, freePos);
        });
    }

    private void addEdges(final Position position, final Set<Position> freePos) {
        final Set<Pair<Position, Direction>> adiax = new HashSet<>(Arrays.asList(
                new Pair<>(new Position(position.getX() + SpriteSheet.SPRITE_SIZE_IN_GAME, position.getY()),
                        Direction.RIGHT),
                new Pair<>(new Position(position.getX() - SpriteSheet.SPRITE_SIZE_IN_GAME, position.getY()),
                        Direction.LEFT),
                new Pair<>(new Position(position.getX(), position.getY() + SpriteSheet.SPRITE_SIZE_IN_GAME),
                        Direction.DOWN),
                new Pair<>(new Position(position.getX(), position.getY() - SpriteSheet.SPRITE_SIZE_IN_GAME),
                        Direction.UP)));
        adiax.stream().filter(
                t -> freePos.stream()
                                   .anyMatch(p -> p.equals(t.getFirst())))
                .forEach(n -> {
                    if (!graph.containsNode(n.getFirst())) {
                        graph.addNode(n.getFirst());
                    }
                    graph.addEdge(position, n.getFirst(), n.getSecond());
                });
    }

}
