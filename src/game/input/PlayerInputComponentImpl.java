package game.input;

import game.graphics.SpriteSheet;
import game.objects.blocks.Bomb;
import game.objects.entities.PlayerImpl;
import game.physics.Direction;
import game.utilities.Position;
import game.world.World;

/**
 * Input component for the player. It implements {@link PlayerInputComponent}.
 */
public class PlayerInputComponentImpl extends AbstractInputComponent implements PlayerInputComponent {

    private static final int PIXEL_PER_SECOND = 4 * SpriteSheet.SPRITE_SIZE_IN_GAME;
    private static final int INITIAL_LIMIT_BOMB = 2;

    private final PlayerImpl player;
    private final int limit;

    /**
     * Creates {@code PlayerInputComponentImpl}.
     *
     * @param player reference
     */
    public PlayerInputComponentImpl(final PlayerImpl player) {
        super(player);
        this.player = player;
        this.limit = INITIAL_LIMIT_BOMB;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void move(final Direction way) {
        super.createDirectionCommand(way, PIXEL_PER_SECOND);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stop() {
        super.createDirectionCommand(Direction.STOP, 0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dropBomb(final Position position) {
        super.createGenericCommand(() -> {
            final World world = this.player.getWorld();
            if (world.getBombs().size() < limit) {
                world.addBomb(new Bomb(position.setInTile(position), world.getTheme().getSprites().getBomb(), world));
            }
        });
    }
}
